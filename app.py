"""barebones flask application"""
import os
import socket

from flask import Flask, render_template, request, jsonify, url_for, redirect
from flask_wtf import FlaskForm
from flask_bootstrap import Bootstrap

from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'


app = Flask(__name__)
app.config.from_object(Config)

bootstrap = Bootstrap(app)


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Home')


@app.route('/post', methods=['POST'])
def post_test():
    # TODO: avoid globals, but use for now
    global labels, values

    data = request.get_json()

    labels_ = data['words']
    values_ = data['data']

    # convert from string representations to actual lists
    l_ = labels_.replace('[', '').replace(']', '').replace('\'', '').split(',')
    v_ = values_.replace('[', '').replace(']', '').split(',')

    # tidy up spaces for labels, and convert to int for values
    labels = [_.strip() for _ in l_]
    values = [int(_) for _ in v_ if _ != '']

    return jsonify(data)


@app.route('/post_sentiment', methods=['POST'])
def post_sentiment():
    # TODO: avoid globals, but use for now
    global pos, neg

    data = request.get_json()

    # convert from string representations to actual lists
    pos_or_neg = data['words'].replace('[', '').replace(']', '').replace('\'', '')
    count = int(data['data'].replace('[', '').replace(']', ''))

    if pos_or_neg == 'pos':
        pos = count
    else:
        neg = count

    return jsonify(data)


@app.route('/counts')
def term_counts():
    # TODO: globals are bad, but we'll use them for now
    global labels, values
    return render_template('term_counts.html', values=values, labels=labels)


@app.route('/sentiment')
def sentiment_counts():
    # TODO: globals are bad, but we'll use them for now
    global pos, neg
    return render_template('sentiment_counts.html', values=[pos, neg], labels=['positive', 'negative'])


@app.route('/update_counts_data')
def refresh_counts_data():
    global labels, values
    return jsonify(labels=labels, values=values)


@app.route('/update_sentiment_data')
def refresh_sentiment_data():
    global pos, neg
    return jsonify(labels=['positive', 'negative'], values=[pos, neg])


@app.route('/search', methods=['GET', 'POST'])
def search():
    form = SearchForm()
    # do the form processing
    if form.validate_on_submit():
        terms = form.terms.data
        print(f'search terms: {terms}')

        # forward the search terms to `streamer.py`
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect(('localhost', 9998))
            # search terms sent as a string - deal with this on
            # the receiving side
            s.sendall(terms.encode('utf-8'))

        return redirect(url_for('index'))

    return render_template('search.html', title='Search', form=form)


class SearchForm(FlaskForm):
    terms = StringField('Search Terms', validators=[DataRequired()])
    submit = SubmitField('Search')
