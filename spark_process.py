"""listen for tweets on a socket, and POST to Flask application"""
import pickle

from nltk.tokenize import word_tokenize
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.sql import Row, SQLContext
import requests
from textblob import TextBlob


def tokenise_tweet(tweet):
    return{word: True for word in word_tokenize(tweet)}


def aggregate_words(values, total):
    return sum(values) + (total or 0)


def get_spark_sql_context_instance(spark_context):
    if ('sqlContextSingletonInstance' not in globals()):
        globals()['sqlContextSingletonInstance'] = SQLContext(spark_context)
    return globals()['sqlContextSingletonInstance']


def process(timestamp, rdd):
    # print(f'---- {timestamp} ----')

    sql_context = get_spark_sql_context_instance(rdd.context)
    row_rdd = rdd.map(lambda w: Row(word=w[0], wcount=w[1]))

    try:
        df = sql_context.createDataFrame(row_rdd)
    except ValueError as e:
        print(e)
    else:
        df.registerTempTable('tweets')
        counts = sql_context.sql('SELECT word, wcount FROM tweets ORDER BY wcount DESC LIMIT 10')
        # counts.show()
        post_data(counts)


def process_posneg(timestamp, rdd):
    # print(f'---- {timestamp} ----')

    sql_context = get_spark_sql_context_instance(rdd.context)
    row_rdd = rdd.map(lambda w: Row(pos_neg=w[0], ntweets=w[1]))

    try:
        df = sql_context.createDataFrame(row_rdd)
    except ValueError as e:
        print(e)
    else:
        df.registerTempTable('sentiment')
        counts = sql_context.sql('SELECT pos_neg, ntweets FROM sentiment')
        # counts.show()
        post_sentiment_data(counts)


def post_sentiment_data(df):
    words = [w.pos_neg for w in df.select('pos_neg').collect()]
    count = [c.ntweets for c in df.select('ntweets').collect()]
    url = 'http://127.0.0.1:5000/post_sentiment'
    data = {'words': str(words), 'data': str(count)}
    headers = {'Content-Type': 'application/json'}
    r = requests.post(url, json=data, headers=headers)


def post_data(df):
    words = [w.word for w in df.select('word').collect()]
    count = [c.wcount for c in df.select('wcount').collect()]
    url = 'http://127.0.0.1:5000/post'
    data = {'words': str(words), 'data': str(count)}
    headers = {'Content-Type': 'application/json'}
    r = requests.post(url, json=data, headers=headers)
    # print(r)


def get_sentiment(data_stream):
    output = data_stream.map(lambda text: (TextBlob(text).sentiment.polarity))
    pos = output.filter(lambda x: x > 0.0).map(lambda x: ('pos', 1))
    neg = output.filter(lambda x: x < 0.0).map(lambda x: ('neg', 1))
    pos_totals = pos.updateStateByKey(aggregate_words)
    pos_totals.foreachRDD(process_posneg)

    neg_totals = neg.updateStateByKey(aggregate_words)
    neg_totals.foreachRDD(process_posneg)


def count_words(data_stream):
    stopwords = {'the', 'to', 'a', 'is', 'of', 'for', 'and', 'in', 'that', 'he', 'was', 'this', 'i', 'with', 'you', 'his', 'on', 'her', 'has', 'be', 'are', 'not', 'it', 'if', 'at', 'have', 'as', 'if', '&amp;', 'by', 'we', 'from', 'an', 'they'}

    # flatMap is one-to-many and generates multiple new records for one input
    words = (data_stream
        .flatMap(lambda text: text.split())
        .map(lambda w: w.lower())
        .filter(lambda w: w not in stopwords)
        .map(lambda w: (w, 1))
    )
    # words.count().pprint()  # prints the number of words, not tweets

    w_totals = words.updateStateByKey(aggregate_words)
    w_totals.foreachRDD(process)


def main():
    """main entry point"""
    # create sc with one thread of execution
    sc = SparkContext(appName='socket_word_count')
    sc.setLogLevel('ERROR')
    # create the SparkStreamingContext `ssc` with a batch interval of 5 seconds
    ssc = StreamingContext(sc, 5)
    ssc.checkpoint('checkpoint_')

    # sentiments = ssc.sparkContext.textFile('AFINN-11.txt').map(lambda line: tuple(line.split('\t')))

    # this is streaming data (DStream) from a tcp source
    data_stream = ssc.socketTextStream('localhost', 9999)

    count_words(data_stream)
    get_sentiment(data_stream)

    ssc.start()
    ssc.awaitTermination()


if __name__ == '__main__':
    # with open('naivebayes_model.pkl', 'rb') as f:
    #     model = pickle.load(f)
    main()
