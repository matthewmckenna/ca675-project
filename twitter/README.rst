#########
streamer
#########

Configuration
==============

You will need your own configuration file to be able to run ``streamer.py``.
This file should be located in the same directory that you run the code from.

You must `create a Twitter application <https://apps.twitter.com>`_ in order to
get your own credentials.

A sample configuration file is present in the repository
(``credentials.ini.sample``). You should copy this file, and rename the copy
``credentials.ini``.

You will then need to edit this ``credentials.ini`` file and populate
the missing fields.

All fields are mandatory. Each field is briefly detailed below:

``[twitter]`` section
---------------------
===============  =======================================================
Key              Description
===============  =======================================================
``app_key``      Consumer Key (API Key) for your application.
``app_sec``      Consumer Secret (API Secret) for your application.
``access_key``   Access Token for your user account.
``access_key``   Access Token Secret for your user account.
===============  =======================================================


Usage
======

To run, execute the following command:

.. code-block:: sh

    $ python streamer.py --track football


Gotchas
========

Do **not** use an equal sign (``=``) when attempting to ``track`` multiple
items.

If you wish to track multiple items, these should be **space separated**.

Examples of **correct** usage of the ``--track`` option:

.. code-block:: sh

    $ python streamer.py --track=football
    $ python streamer.py --track football hurling
    $ python streamer.py -t football
    $ python streamer.py -t football hurling

Examples of **incorrect** usage of the ``--track`` option:

.. code-block:: sh

    $ python streamer.py --track=football hurling
    $ python streamer.py --track="football hurling"
    $ python streamer.py -t=football hurling
    $ python streamer.py -t="football hurling"
