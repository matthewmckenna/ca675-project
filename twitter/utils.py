"""utility functions for twitter api access."""
import configparser
from datetime import datetime
import os
import string

import tweepy


def get_directory_path(directory):
    """Get an absolute path to the output directory."""
    # check if there is a `~` in the output dir specified by the user.
    # if so, expand the path
    if '~' in directory:
        path = os.path.expanduser(directory)
    else:
        path = os.path.abspath(directory)

    return path


def authenticate(creds):
    """Authenticate with the Twitter API service.

    Args:
        creds: credentials returned from configparser
    Returns:
        auth: tweepy.OAuthHandler object
    """
    auth = tweepy.OAuthHandler(creds['app_key'], creds['app_sec'])
    auth.set_access_token(creds['access_key'], creds['access_sec'])
    return auth


def read_api_key(credentials):
    """Return API key from configuration file."""
    config = configparser.ConfigParser()
    config.read(credentials)
    return config['twitter']


def construct_safe_filename(track=None):
    """Construct a 'safe' alphanumeric filename.

    Creates a suitable filename which only contains characters which
    are deemed 'valid' in `get_safe_string`.

    Trims filenames which are too long, to 40 characters plus a
    timestamp.

    Args:
        track: list of strings

    Returns:
        string filename
    """
    if track is None:
        track = []

    timestamp = datetime.now().strftime('%Y%m%d_%H%M')

    # max filename length is n + length of underscore and timestamp
    max_filename_characters = 40 + len(f'_{timestamp}')

    track_string = '_'.join(t for t in track)
    # remove symbols from the tracked items when creating a filename
    track_string = track_string.replace('#', '')
    track_string = track_string.replace('@', '')

    # get a safe representation of the string
    s = get_safe_string(track_string)

    # limit the length of the filename
    if len(s) > max_filename_characters:
        s = s[:max_filename_characters]

    return '{}_{}.jsonl'.format(s, timestamp)


def convert_to_valid(c):
    """Convert a character `c` into an allowed character.

    Args:
        c: a character from a string

    Returns:
        `c` if `c` is valid, otherwise return an underscore '_'.
    """
    valid_characters = '{}{}.-_'.format(string.ascii_letters, string.digits)

    if c in valid_characters:
        return c
    else:
        return '_'


def get_safe_string(s):
    """Transform a string into a 'safe' representation."""
    safe_s = ''.join(convert_to_valid(c) for c in s)

    return safe_s.lower()
