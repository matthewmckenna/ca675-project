#!/usr/bin/env python
"""Simple application to save tweets from the Twitter Streaming API."""
from http.client import IncompleteRead
import socket

import tweepy

from utils import authenticate, read_api_key


class SimpleStreamListener(tweepy.StreamListener):
    """Custom listener for Twitter streaming data."""

    def __init__(self, connection):
        super().__init__()
        self.tweets_seen = 0
        self.socket = connection

    def on_status(self, status):
        self.send_to_socket(status)

        return True

    def send_to_socket(self, status):
        """send the tweet over a tcp connection"""
        self.tweets_seen += 1

        if (self.tweets_seen % 10) == 0:
            print('seen {} tweets'.format(self.tweets_seen))

        if 'retweeted_status' in status._json:
            base_tweet = status._json['retweeted_status']
        else:
            base_tweet = status._json

        if 'extended_tweet' in base_tweet:
            tweet = base_tweet['extended_tweet']['full_text']
            # print('LONG ', end='')
        else:
            tweet = base_tweet['text']

        tweet = tweet.replace('\n', ' ') + '\n'
        # print(f"sent: {repr(tweet)} length: {len(tweet)}")
        self.socket.send(tweet.encode('utf-8'))

    def on_error(self, status_code):
        if status_code == 420:
            print('rate limit exceeded')
            return False


def main():
    """Main entry point for this script."""
    auth = authenticate(read_api_key('credentials.ini'))

    # get the terms to search
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind(('localhost', 9998))
        s.listen(1)
        conn, addr = s.accept()
        with conn:
            s_terms = conn.recv(1024).decode('utf-8')
            print(f'the search terms are {repr(s_terms)}')

    # tracking terms must be a list, so convert the received string
    if ',' in s_terms:
        terms = [t.strip() for t in s_terms.split(',')]
    else:
        terms = [s_terms]

    # establish the socket for sending to spark
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost', 9999))
    print('waiting for connection to spark')
    s.listen(1)
    conn, addr = s.accept()
    print('connected')

    stream_tweets(auth, conn, terms)


def stream_tweets(auth, connection, terms):
    """Get tweets using the Streaming API."""
    api = tweepy.API(
        auth,
        wait_on_rate_limit=True,
        wait_on_rate_limit_notify=True,
    )

    stream_listener = SimpleStreamListener(connection)
    stream = tweepy.Stream(auth=api.auth, listener=stream_listener)

    try:
        stream.filter(track=terms, stall_warnings=True)
    # in case we cannot keep up with the rate at which tweets are
    # being generated
    except (ConnectionResetError, IncompleteRead, socket.timeout) as e:
        print(e)
        stream.disconnect()
    except KeyboardInterrupt:
        print('exiting.')
        stream.disconnect()


if __name__ == '__main__':
    main()
