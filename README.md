# CA675 Assignment 2 - Group A

*Work in progress*

---

Example usage:

The following instructions assume that Spark/PySpark is configured, the Python version is > 3.6 and the Python libraries in `requirements.txt` are installed (`pip install -r requirements.txt`)

1. From a terminal run `python streamer.py -t trump`.
2. From a second terminal run `spark-submit spark_process.py`.
3. In a third terminal, set the `FLASK_APP` environment variable as: `export FLASK_APP=app.py` on Unix-like systems, or `set FLASK_APP=app.py` on Windows.
4. From this terminal run `flask run`. This will launch the Flask application.
